makepasswd (1.10-15) UNRELEASED; urgency=medium

  * Apply X-Style: black.

 -- Colin Watson <cjwatson@debian.org>  Mon, 08 Jul 2024 13:46:49 +0100

makepasswd (1.10-14) unstable; urgency=medium

  * Split up Debian patches.

 -- Colin Watson <cjwatson@debian.org>  Sat, 17 Dec 2022 12:43:15 +0000

makepasswd (1.10-13.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Convert to 3.0 source format.

 -- Bastian Germann <bage@debian.org>  Sat, 03 Dec 2022 00:36:48 +0100

makepasswd (1.10-13) unstable; urgency=medium

  [ Colin Watson ]
  * Use debhelper-compat instead of debian/compat.

  [ Debian Janitor ]
  * Drop no longer supported add-log-mailing-address setting from
    debian/changelog.
  * Bump debhelper from old 10 to 12.
  * Remove constraints unnecessary since buster:
    + makepasswd: Drop versioned constraint on perl in Depends.

 -- Colin Watson <cjwatson@debian.org>  Sun, 26 Dec 2021 13:40:04 +0000

makepasswd (1.10-12) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces

  [ Colin Watson ]
  * Set Rules-Requires-Root: no.
  * Upgrade to debhelper v10.
  * Convert debian/copyright to copyright-format 1.0.
  * Policy version 4.2.1.

 -- Colin Watson <cjwatson@debian.org>  Sat, 03 Nov 2018 09:41:00 +0000

makepasswd (1.10-11) unstable; urgency=medium

  * Switch to git; update Vcs-* fields.
  * Use /dev/urandom as intended and documented, not /dev/random
    (LP: #1573974).

 -- Colin Watson <cjwatson@debian.org>  Sat, 10 Feb 2018 02:04:29 +0000

makepasswd (1.10-10) unstable; urgency=low

  * Move debian/source.lintian-overrides to preferred location of
    debian/source/lintian-overrides.
  * Convert from Crypt::OpenSSL::Random to Bytes::Random::Secure (closes:
    #792535).

 -- Colin Watson <cjwatson@debian.org>  Thu, 16 Jul 2015 11:45:20 +0100

makepasswd (1.10-9) unstable; urgency=low

  * Fix output formatting when --clearfrom password is >= 12 characters
    (thanks, Steven Van Acker; LP: #894739).
  * Remove redundant debian/dirs file.
  * Canonicalise Vcs-Bzr and Vcs-Browser URLs.
  * Override debian-watch-file-is-missing Lintian message.
  * Explicitly set source format to 1.0 for now.
  * Policy version 3.9.4: no changes required.

 -- Colin Watson <cjwatson@debian.org>  Mon, 27 May 2013 23:43:43 +0100

makepasswd (1.10-8) unstable; urgency=low

  * debian/copyright: Note that the upstream source location no longer
    exists due to the upstream author's death some years ago (closes:
    #660962).

 -- Colin Watson <cjwatson@debian.org>  Wed, 07 Mar 2012 10:25:17 +0000

makepasswd (1.10-7) unstable; urgency=low

  * Remove Linux-specific text from the package description, the manual
    page, and the copyright file (closes: #642410).

 -- Colin Watson <cjwatson@debian.org>  Thu, 22 Sep 2011 15:11:39 +0100

makepasswd (1.10-6) unstable; urgency=low

  * Policy version 3.8.4: no changes required.
  * Add CVE entry to previous changelog stanza.
  * Send --help output to stdout, not stderr.

 -- Colin Watson <cjwatson@debian.org>  Sat, 28 May 2011 09:31:32 +0100

makepasswd (1.10-5) unstable; urgency=low

  * Imported into a branch on bzr.debian.org; add Vcs-Bzr and Vcs-Browser
    control fields.
  * Use OpenSSL's random number generator, seeded with 256 bits of entropy
    from /dev/urandom (CVE-2010-2247; closes: #564559).

 -- Colin Watson <cjwatson@debian.org>  Mon, 22 Feb 2010 00:39:50 +0000

makepasswd (1.10-4) unstable; urgency=low

  * Upgrade to debhelper v7.
  * Override a Lintian warning about syntax in an old changelog entry; I'm
    not going to rewrite history for this.
  * Policy version 3.8.2. No changes required.
  * Use /dev/urandom rather than /dev/random, as the latter is overkill for
    this and drains entropy too quickly (thanks, Ralf Hildebrandt; closes:
    #307700).
  * Increase default password length range from 6-8 characters to 8-10
    (closes: #23648).

 -- Colin Watson <cjwatson@debian.org>  Fri, 14 Aug 2009 22:31:13 +0100

makepasswd (1.10-3) unstable; urgency=low

  * Open /dev/random with just the :unix layer to avoid draining it in
    4096-byte buffered chunks (closes: #320310). Requires perl 5.8 for
    PerlIO.
  * Use dh_installman rather than the deprecated dh_installmanpages.
  * Remove SHELL=/bin/bash and other cruft in debian/rules.
  * Upgrade to debhelper v4.
  * Policy version 3.6.2. No changes required.

 -- Colin Watson <cjwatson@debian.org>  Sun,  7 Aug 2005 17:48:40 +0100

makepasswd (1.10-2) unstable; urgency=low

  * New maintainer (closes: #192660).
  * Acknowledge Javier's NMU; thanks!
  * makepasswd: Fix --crypt-md5 passwords so that PAM actually accepts them,
    using Crypt::PasswdMD5 (closes: #44788). --crypt-md5 can now be used
    with --repeatpass.
  * makepasswd: Document --crypt-md5 in --help output.
  * debian/control (Description): Change "on the command line" (--clear) to
    "in a temporary file" (--clearfrom).
  * debian/control: Build-depend on debhelper (>= 3.0.18), per the Perl
    policy. Move this from Build-Depends-Indep to Build-Depends since
    Build-Depends-Indep doesn't have to be satisfied during clean.
  * debian/rules: Modernize a bit.
  * debian/control (Standards-Version): Bump to 3.6.1.

 -- Colin Watson <cjwatson@debian.org>  Mon, 25 Aug 2003 05:05:43 +0100

makepasswd (1.10-1.1) unstable; urgency=low

  * Non-maintainer upload.
    Since the rules have changed and this package has not
    (since potato) I'm uploading to 0-delay. This upload will
    not fix any RC bugs but at least will (almost) remove all
    the bugs open for this package, and it didn't take me
    much time to figure these out...
    - Change program name so that the help text is displayed
      properly (Closes: #147808)
    - Now Build-Depends-Indep from debhelper (Closes: #190485)
    (I'm not bumping up the Standards Version since this should
    be revised by the maintainer)
    - Using --clear now exits with error warning the user that
      the option is no longer valid (Closes: #50885)
    - Added 'use bytes' as suggested by reporter to be UTF-8 clean
      (although I'm not sure if this bug applies any longer since
      I cannot reproduce it, in any case, using that module
      shouldn't, hopefully, break anything. (Closes: #168492)
    - Generate MD5 passwords with the --crypt-md5 option (Closes: #44788)

 -- Javier Fernandez-Sanguino Pen~a <jfs@computer.org>  Wed, 20 Aug 2003 02:40:37 +0200

makepasswd (1.10-1) unstable; urgency=low

  * New upstream version, now possible to pass the cleartext in a file,
    closes: #31059.
  * Corrected maintainer address (should be johnie@debian.org).

 -- Johnie Ingram <johnie@debian.org>  Sat, 18 Sep 1999 03:23:37 -0500

makepasswd (1.07-3) unstable; urgency=low

  * Updated to Standards-Version: 3.0.1.0, closes: #41502.

 -- Johnie Ingram <johnie@netgod.net>  Mon, 30 Aug 1999 10:36:28 -0500

makepasswd (1.07-2.1) unstable; urgency=low

  * NMU for the perl upgrade. Closes: #41502
  * Changed the dependency: s/perl/perl5/
  * Upgraded standards-version to 2.5.1
  * Corrected the location of the GPL in the copyright file.
  * Installed the man page instead of the undocumented link.

 -- Raphael Hertzog <rhertzog@hrnet.fr>  Wed, 21 Jul 1999 19:05:00 +0200

makepasswd (1.07-2) unstable; urgency=low

  * Correct debian/rules target (binary-indep) used.
  * Switched from debmake to debhelper packaging technology.
  * Updated to Standards-Version 2.5.0.0.

 -- Johnie Ingram <johnie@debian.org>  Mon, 21 Dec 1998 15:55:46 -0500

makepasswd (1.07-1) unstable; urgency=low

  * New upstream version (which includes --string fix, cf. #15759)
  * Added upstream changelog file and README.
  * Now has pristine source archive.

 -- Johnie Ingram <johnie@debian.org>  Mon,  9 Feb 1998 11:53:26 -0500

makepasswd (1.06-2) unstable; urgency=low, closes=15759

  * Updated year and other details in copyright file.
  * Patched makepasswd so --string support works (#15759).
  * Updated to Standards-Version 2.4.0.0.

 -- Johnie Ingram <johnie@debian.org>  Mon,  9 Feb 1998 10:59:50 -0500

makepasswd (1.06-1) unstable; urgency=low

  * Initial Release.

 -- Johnie Ingram <johnie@debian.org>  Sun,  9 Nov 1997 04:30:59 -0500
