From fb75b084eda1baaea4802b220c8f8d911fd9e43c Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Javier=20Fernandez-Sanguino=20Pe=C3=B1a?= <jfs@computer.org>
Date: Sat, 17 Dec 2022 12:23:05 +0000
Subject: Generate MD5 passwords with the --crypt-md5 option

Author: Colin Watson <cjwatson@debian.org>
Bug-Debian: https://bugs.debian.org/44788
Forwarded: no
Last-Update: 2022-12-17

Patch-Name: crypt-md5.patch
---
 makepasswd   | 107 ++++++++++++++++++++++++++++++++++++++++++++++-----
 makepasswd.1 |   9 +++--
 2 files changed, 103 insertions(+), 13 deletions(-)

diff --git a/makepasswd b/makepasswd
index 30d23e5..46c4c9a 100755
--- a/makepasswd
+++ b/makepasswd
@@ -29,6 +29,7 @@ $Chars = "";
 $Clear = "";
 $Count = "";
 $Crypt = 0;
+$CryptMd5 = 0;
 $CryptSalt = "";
 $MaxChars = "";
 $MinChars = "";
@@ -96,6 +97,7 @@ $SaltList="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789./";
 	'clearfrom=s'		=> \$Clear,
 	'count=i'		=> \$Count,
 	'crypt!'		=> \$Crypt,
+	'crypt-md5!'		=> \$CryptMd5,
 	'cryptsalt=i'		=> \$CryptSalt,
 	'help'			=> \$ShowHelp,
 	'maxchars=i'		=> \$MaxChars,
@@ -147,10 +149,10 @@ $Clear ne "" and do
 			"--chars --minchars --maxchars --count --string.\n";
 		$Error = 1;
 	};
-	$Crypt or do
+	$Crypt or $CryptMd5 or do
 	{
 		print STDERR "$Program:  Option --clearfrom may not be specified ".
-			"without option --crypt.\n";
+			"without option --crypt or --crypt-md5.\n";
 		$Error = 1;
 	};
 	open CLEARFROM, "$Clear" or do
@@ -269,9 +271,9 @@ $RepeatPass ne "" and do
 # If --crypt is not set or --cryptsalt is set, disallow this parameter.
 #
 
-	$Crypt or do
+	$Crypt or $CryptMd5 or do
 	{
-		print STDERR "$Program:  To use --repeatpass, --crypt must also be set.\n";
+		print STDERR "$Program:  To use --repeatpass, --crypt or --crypt-md5 must also be set.\n";
 		$Error = 1;
 	};
 	$CryptSalt and do
@@ -463,12 +465,14 @@ Options are:
 --chars=N        Generate passwords with exactly N characters (do not use with
                        options --minchars and --maxchars).
 --clearfrom=FILE Use a clear password from FILE instead of generating passwords.
-                       Requires the --crypt option; may not be used with options
-                       --chars, --maxchars, --minchars, --count, --string,
-                       --nocrypt.  Trailing newlines are ignored, other
-                       whitespace is not.
+                       Requires the --crypt or --crypt-md5 option; may not be
+                       used with options --chars, --maxchars, --minchars,
+                       --count, --string, --nocrypt.  Trailing newlines are
+                       ignored, other whitespace is not.
 --count=N        Produce a total of N passwords (the default is one).
 --crypt          Produce encrypted passwords.
+--crypt-md5      Produce encrypted passwords using the MD5 digest (hash)
+                       algorithm.
 --cryptsalt=N    Use crypt() salt N, a positive number <= 4096.  If random
                        seeds are desired, specify a zero value (the default).
 --help           Ignore other operands and produce only this help display.
@@ -482,8 +486,8 @@ Options are:
                        to use a single seed value (the default).  Specify
                        one to get true-random passwords, but plan on hitting
                        the CONTROL key a lot while it's running. ;)
---repeatpass=N   Use each password N times (4096 maximum, --crypt must be set
-                       and --cryptsalt may not be set).
+--repeatpass=N   Use each password N times (4096 maximum, --crypt or
+                       --crypt-md5 must be set and --cryptsalt may not be set).
 --string=STRING  Use the characters in STRING to generate random passwords.
 --verbose        Display labelling information on output.
 ";
@@ -600,6 +604,82 @@ sub CryptPassword
 	crypt($_[0], MakeSalt($ThisSeed));
 }
 
+#
+# sub Md5Base64Char(A): Base-64-encode a character from an MD5 digest.
+#
+
+sub Md5Base64Char
+{
+	my $map64 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
+	my $char = shift;
+	$char = 0 if $char < 0;
+	$char = 63 if $char > 63;
+	substr($map64, $char, 1);
+}
+
+#
+# sub MakeMd5Salt(A, B):  Generate a crypt salt string from a number from 0
+# through 4095.
+#
+
+sub MakeMd5Salt
+{
+	my $md5 = Digest::MD5->new();
+	$md5->add(time);
+	$md5->add($$);
+	$md5->add($_[0]);
+	$md5->add($_[1]);
+	my $digest = $md5->digest;
+	$digest = substr($digest, 0, 8);
+	my $salt;
+	for my $char (map { ord($_) & 077 } split //, $digest)
+	{
+		$salt .= Md5Base64Char($char);
+	}
+	$salt;
+}
+
+#
+# sub CryptMd5Password(A, B):  Encrypt the password provided using the
+# MD5 digest algorithm; keep a running list of codes used as long as B is
+# true.
+#
+
+sub CryptMd5Password
+{
+	my $password = $_[0];
+	eval "use Crypt::PasswdMD5";
+	if ($@)
+	{
+		print STDERR "$Program:  Could not load the Crypt::PasswdMD5 library, cannot use --crypt-md5\n".
+			"This may be due to an invalid or incomplete Perl installation\n.";
+		exit 1;
+	};
+
+	my $ThisSeed = $SeedValue;
+	if ($ThisSeed)
+	{
+		$ThisSeed--;
+	}
+	else
+	{
+		$_[1] or do
+		{
+			%UsedSeed = ();
+		};
+		$ThisSeed = Random(0, 4095);
+		do
+		{
+			$ThisSeed = Random(0, 4095);
+		}
+		until not exists $UsedSeed{$ThisSeed};
+		$UsedSeed{$ThisSeed} = $ThisSeed;
+	}
+
+	my $salt = MakeMd5Salt($password, $ThisSeed);
+	unix_md5_crypt($password, $salt);
+}
+
 #
 # sub ProcessPassword(A):  Process the password provided.
 #
@@ -613,6 +693,7 @@ sub ProcessPassword
 	
 	$Password = MakePassword();
 	$Crypt and $PaddedPass = sprintf "%-$CharFormat"."s", $Password;
+	$CryptMd5 and $PaddedPass = sprintf "%-$CharFormat"."s", $Password;
 	$Verbose and do
 	{
 		$PassLabel="Password=";
@@ -629,6 +710,12 @@ sub ProcessPassword
 			print "$PassLabel"."$PaddedPass"."$CryptLabel"."$CryptedPass\n";
 			$Verbose and $PaddedPass = $EmptyPassword;
 		}
+		elsif ($CryptMd5) 
+		{
+			$CryptedPass = CryptMd5Password($Password);
+			print "$PassLabel"."$PaddedPass"."$CryptLabel"."$CryptedPass\n";
+			$Verbose and $PaddedPass = $EmptyPassword;
+		}
 		else
 		{
 			print "$PassLabel"."$Password\n";
diff --git a/makepasswd.1 b/makepasswd.1
index 9c67141..d8250c5 100644
--- a/makepasswd.1
+++ b/makepasswd.1
@@ -33,7 +33,7 @@ makepasswd \- generate and/or encrypt passwords
 .I N
 ]
 [
-.B \--crypt | --nocrypt
+.B \--crypt | --nocrypt | --crypt-md5
 ]
 [
 .B \--cryptsalt
@@ -83,8 +83,8 @@ Generate passwords with exactly N characters (do not use with options
 .TP
 .B --clearfrom FILE
 Use password from FILE instead of generating passwords.  Requires
-the --crypt
-option; may not be used with these options: --chars, --maxchars, --minchars,
+the --crypt or the --crypt-md5
+options; may not be used with these options: --chars, --maxchars, --minchars,
 --count, --string, --nocrypt.  Trailing newlines are removed but other white
 space is not.
 .TP
@@ -94,6 +94,9 @@ Produce a total of N passwords (the default is one).
 .B --crypt
 Produce encrypted passwords.
 .TP
+.B --crypt-md5
+Produce encrypted passwords using the MD5 digest (hash) algorithm.
+.TP
 .B --cryptsalt N
 Use crypt() salt N, a positive number <= 4096.  If random seeds are
 desired, specify a zero value (the default).
